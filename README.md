# LÉAME #

Continuamos en el camino de aprender a usar Git, un sistema de versionado con sus respectivos comandos básicos, los cuales  facilitan realizar diferentes acciones, que ayudan a llevar el historial completo de modificaciones de un proyecto.


Algunos ejemplos son;

GIT INIT, inicia un nuevo repositorio.

GIT ADD, añade un archivo a la zona de montaje.

GIT LOG, se usa para listar el historial de versiones de la rama actual.

GIT RESET, descompone el archivo, pero conserva el contenido del mismo.

GIT CLONE, clona un repositorio existente.

GIT CONFIG, establece el nombre del autor, el correo y otros parámetros que Git utiliza.

GIT STATUS, enumera todos los archivos que deben ser configurados.

GIT DIFF, muestra las diferencias de archivo que aún no se ponen en escena.

GIT HELP, muestra información para saber el uso de cada comando de Git.

GIT CLEAN, elimina archivos no deseados de un reposistorio.

GIT BRANCH, muestra la lista de ramas que existen en un repositorio.

GIT MERGE, fusiona dos o más ramas.

GIT PULL, descarga y actualiza los cambios realizados desde un repositorio remoto a tu repositorio local.

GIT PUSH, sube los archivos a un repositorio remoto.

GIT CHECKOUT, sirve para moverse de una rama a otra y regresar en el tiempo.

GIT REMOTE, crea, visualiza y elimina conexiones a otros repositorios.

GIT COMMIT, agrega el archivo a la base de datos.

GIT COMMIT-m, sirve para agregar comentarios, notas, etc.

GIT REVERT, deshace los commits hechos.

GIT TAG, marca commits específicos.

GIT LOG, muestra el historial del repositorio listando ciertos detalles de la confirmación.






